var $ = require('jquery');

/**
 * ボタンのDOM差し込み
 */
$('body').append(
  $('<div>',
    {
      css: {
        position: 'fixed',
        bottom: '2px',
        right: '2px',
        maxWidth: '100px',
        maxHeight: '100px',
        backgroundColor: 'white',
        borderRadius: '4px',
        border: 'solid #ccc 1px',
        padding: '4px'
      }
    }
  ).append(
    $('<button>',
      {
        'class': 'btn btn-primary btn-xs js-batch',
        text: 'まとめてDL'
      }
    )
  )
);

$('body').on(
  'click',
  '.js-batch',
  function(e) {
    $('.panel-post').each(function() {

      var $images = $('.posted-image', this);
      var date = $('.post-created', this)
      .text()
      .replace(/-/g, '')
      .replace(' ', '_')
      .replace(/\:/g, '');
      var pid = $('.post-id', this).text()
      .trim()
      .split('_')[0];

      $images.each(function(index) {
        var url = new URL($(this).attr('src'));
        var filename = url.pathname.split('/').pop();
        var ext = filename.split('.').pop();

        var opts = {
          url: $(this).attr('src'),
          filename: [pid, date, index].join('-') + '.' + ext
        };

        chrome.runtime.sendMessage(opts);
      });
    });
  }
);
